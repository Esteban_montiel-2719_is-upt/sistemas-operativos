using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace trivia
{
    public partial class Prueba : Form
    {
        int resultado = 0;
        int seleccion = 1;
        public Prueba()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            Iniciar();
        }

        private void Iniciar()
        {
            if (MessageBox.Show("Bienvenido a este test si necesitas ayuda arriba en el menu escontraras informacion variada", "Test sistemas operativos", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                this.pregunta.Text = "Se concidera mas dificil de aprender y utilizar que las interfaces graficas: ";
                this.opcion1.Text = "Sistema Operativo";
                this.opcion2.Text = "Consola ";
                this.opcion3.Text = " Monotarea";
                this.opcion4.Text = "Nucleo";
            }
            else
            {
                Application.Exit();
            }

        }

        private void pregunta2()
        {
            this.pregunta.Text = "Es un conjunto de procedimientos manuales y  automaticos ";
            this.opcion1.Text = "Sistema Operativo";
            this.opcion2.Text = "Consola";
            this.opcion3.Text = "Multitarea";
            this.opcion4.Text = "Interprete de comando";
            seleccion++;
        }
        private void pregunta3()
        {
            this.pregunta.Text = "Es el tipo de visualizacion que permite al usuario elegir un comando";
            this.opcion1.Text = "Sistemas con interfaz grafica ";
            this.opcion2.Text = "Sistemas con interfaz de linea";
            this.opcion3.Text = "Consola";
            this.opcion4.Text = "Nucleo";
            seleccion++;
        }
        private void pregunta4()
        {
            this.pregunta.Text = "Contiene modulos para aceptartantos datos como instruccionesy obtener los resultados ";
            this.opcion1.Text = "Consola";
            this.opcion2.Text = "Unidad de memoria";
            this.opcion3.Text = "Componente basico";
            this.opcion4.Text = "Registro interno";
            seleccion++;
        }
        private void pregunta5()
        {
            this.pregunta.Text = "Es el modo de funcionamiento dsiponible en algunos SO donde la computadora procesavarias tareas ";
            this.opcion1.Text = "Sistema Operativo de red";
            this.opcion2.Text = "Monotareas";
            this.opcion3.Text = "Gestor de archivos";
            this.opcion4.Text = "Multitareas";
            seleccion++;
        }
        private void pregunta6()
        {
            this.pregunta.Text = "Son aquellos que solo atienden un usuario debido a limitaciones";
            this.opcion1.Text = "SO";
            this.opcion2.Text = "Multitareas";
            this.opcion3.Text = "Monousuario";
            this.opcion4.Text = "Ninguna de las anteriores";
            seleccion++;
        }
        private void pregunta7()
        {
            this.pregunta.Text ="El SO esta compuesto por un---de sw que puede utilizarse para gestionar las interacciones con el hardware";
            this.opcion1.Text = "SO";
            this.opcion2.Text = "Gestion de archivos";
            this.opcion3.Text = "Unidad de memoria";
            this.opcion4.Text = "Conjunto de paquetes";
            seleccion++;
        }
        private void pregunta8()
        {
            this.pregunta.Text = "Cual es el comando para guardar un archivo en debian";
            this.opcion1.Text = " HelpDesk";
            this.opcion2.Text = "nano";
            this.opcion3.Text = "Ctrl O";
            this.opcion4.Text = "Ctrl X";
            seleccion++;
        }
        private void pregunta9()
        {
            this.pregunta.Text = "Cual es el comando para salir de un archivo en debian";
            this.opcion1.Text = "Ctrl X";
            this.opcion2.Text = "Ctrl O";
            this.opcion3.Text = "Ctrl L";
            this.opcion4.Text = "Ctrl G";
            seleccion++;
        }
        private void pregunta10()
        {
            this.pregunta.Text = "Que directorio se ocupa para archivos de configuracion de sistema en debian";
            this.opcion1.Text = "/Etc";
            this.opcion2.Text = "/Proc";
            this.opcion3.Text = "/Srv";
            this.opcion4.Text = "/Dev";
            seleccion++;
        }
        private void Limpiar() //limpi las pantallas de los paneles
            
        {
            this.opcion1.Checked = true;//Inicia el radiobutton en la primera opcion en todos los panels 
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //COMIENZA EL COMPARADOR PARA RESPUESTAS 
            switch (seleccion)
            {
                case 1:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta2();
                    Limpiar();
                    break;
                    
                case 2:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta3();
                    Limpiar();
                    break;

                case 3:
                    if (this.opcion4.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta4();
                    Limpiar();
                    break;

                case 4:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta5();
                    Limpiar();
                    break;

                case 5:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta6();
                    Limpiar();
                    break;

                case 6:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta7();
                    Limpiar();
                    break;

                case 7:
                    if (this.opcion4.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta8();
                    Limpiar();
                    break;

                case 8:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta9();
                    Limpiar();
                    break;

                case 9:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    pregunta10();
                    Limpiar();
                    break;

                case 10:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    if (resultado >= 4)
                    {
                        MessageBox.Show(resultado.ToString(), "Felicidades acabaste con exito este test", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Application.Exit();
                    }
                    else
                    {
                        MessageBox.Show(resultado.ToString(), "Debes poner mas atencion y estudiar mas tu total de aciertos fue: ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Application.Exit();
                    }

                    break;
                default:
                    break;
            }
        }

        private void opcion1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
